package main

import (
	"fmt"
	"io"
	"path/filepath"

	"github.com/chzyer/readline"

	"bufio"
	"os"
	"os/user"
	"strings"
)

const (
	MaxItemSize = 80
)

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "usage: %v filename-with-inputs\n", filepath.Base(os.Args[0]))
		return
	}

	var opts []readline.PrefixCompleterInterface
	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	r := bufio.NewReader(f)
	for line, err := r.ReadString('\n'); ; line, err = r.ReadString('\n') {
		if err != nil {
			if err != io.EOF {
				panic(err)
			}
			break
		}
		line = strings.TrimSpace(line)
		if len(line) > MaxItemSize {
			line = line[:MaxItemSize]
		}
		opts = append(opts, readline.PcItem(line))
	}
	f.Close()

	completer := readline.NewPrefixCompleter(opts...)

	cfg := &readline.Config{
		Prompt:            "\033[31m»\033[0m ",
		AutoComplete:      completer,
		InterruptPrompt:   "^C",
		HistorySearchFold: true,
		Stdout:            os.Stderr,
	}

	configHome := ""
	// default to ~/.config/dmenucli
	if usr, err := user.Current(); err != nil {
		if usr.HomeDir != "" {
			configHome = usr.HomeDir + "/.config/dmenucli"
		}
	}

	// can we be more specific?
	configHomeXDG, ok := os.LookupEnv("XDG_CONFIG_HOME")
	if ok && configHomeXDG != "" {
		configHome = configHomeXDG + "/.config/dmenucli"
	}

	if configHome != "" {
		if err := os.MkdirAll(configHome, 0700); err != nil {
			panic(err)
		}
		cfg.HistoryFile = configHome + "/history"
	}
	l, err := readline.NewEx(cfg)
	if err != nil {
		panic(err)
	}

	ln, err := l.Readline()
	if err == nil {
		fmt.Printf("%s\n", strings.TrimSpace(ln))
	} else {
		panic(err)
	}
}
